package jp.atomitech.game.demo.thrift;

import org.apache.thrift.TException;

public class HelloWorldHander implements HelloWorld.Iface{

	@Override
	public String hello(String name) throws TException {
		System.out.println("Here is hello method!  get name is " + name);
	    return "Hello," + name + "!!";
	}

}
